﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms77229120
{
    public class CmdParse
    {

        /// <summary>
        /// variable declaration
        /// </summary>
        private static int x = 0, y = 0;
        private static Color color = Color.Black;
        public static List<String> list = new List<string>();
        public static int fill = 0;
        public static List<String> mesglst = new List<string>();

        /// <summary>
        /// method to parse command
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="g"></param>
        /// <returns></returns>
        public static bool parse(string cmd, Graphics g)
        {
            list.Clear();
            bool valid = false;
            ShapeFactory factory = new ShapeFactory();
            try
            {
                string[] args = cmd.Split(new[] { ' ' });
                if (args.Length > 1)
                {
                    String command = args[0].ToUpper().Trim();
                    switch (command)
                    {
                        case "":
                            list.Add("Command is required");
                            break;
                        case "DRAW":
                            switch (args[1].ToUpper().Trim())
                            {
                                case "":
                                    list.Add("Command is required");
                                    break;
                                case "CIRCLE":
                                    valid = drawCircle(args[2], factory, g);
                                    break;
                                case "RECTANGLE":
                                    valid = drawRectangle(args[2], factory, g);
                                    break;
                                case "TRIANGLE":
                                    valid = drawTriangle(args[2], factory, g);
                                    break;
                                default:
                                    list.Add("Command is required");
                                    break;
                            }
                            break;
                        case "MOVETO":
                            x = int.Parse(args[1].Split(',')[0].Trim());
                            y = int.Parse(args[1].Split(',')[1].Trim());
                            valid = true;
                            break;
                        case "PEN":
                            switch (args[1].ToUpper().Trim())
                            {
                                case "":
                                    valid = false;
                                    list.Add("Command is required");
                                    break;
                                case "RED":
                                    color = Color.FromName("red");
                                    valid = true;
                                    break;
                                case "BLUE":
                                    color = Color.FromName("blue");
                                    valid = true;
                                    break;
                                case "GREEN":
                                    color = Color.FromName("green");
                                    valid = true;
                                    break;
                                default:
                                    valid = false;
                                    list.Add("Command is required");
                                    break;
                            }
                            break;
                        case "FILL":
                            switch (args[1].ToUpper().Trim())
                            {
                                case "":
                                    valid = false;
                                    list.Add("Command is required");
                                    break;
                                case "ON":
                                    fill = 1;
                                    valid = true;
                                    break;
                                case "OFF":
                                    fill = 0;
                                    valid = true;
                                    break;
                                default:
                                    valid = false;
                                    list.Add("Command is required");
                                    break;
                            }

                            break;
                        default:
                            valid = false;
                            list.Add("Command is required");
                            break;
                    }
                }
                else
                {
                    valid = false;
                    list.Add("Command is required");

                }
            }
            catch (FormatException)
            {
                valid = false;
                list.Add("Command is required");

            }
            catch (Exception)
            {
                valid = false;
                list.Add("Command is required");
            }
            return valid;
        }


        /// <summary>
        /// method to clear shape
        /// </summary>
        public static void clearShape()
        {
            x = 0;
            y = 0;
            color = Color.Black;
        }


        /// <summary>
        /// method to draw circle
        /// </summary>
        /// <param name="radiu"></param>
        /// <param name="sfactory"></param>
        /// <param name="g"></param>
        /// <returns></returns>
        static bool drawCircle(String radiu, ShapeFactory sfactory, Graphics g)
        {
            bool valid = false;
            if (!String.IsNullOrEmpty(radiu) && !radiu.Contains(","))
            {
                int radius = int.Parse(radiu.Trim());
                Shape circle = sfactory.getShape("CIRCLE");
                circle.set(color, x, y, radius);
                circle.draw(g, fill);
                valid = true;
            }
            else
            {
                list.Add("Radius required,please write radius.");
            }
            return valid;
        }


        /// <summary>
        /// method to draw rectangle
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="sfactory"></param>
        /// <param name="g"></param>
        /// <returns></returns>
        static bool drawRectangle(String parameters, ShapeFactory sfactory, Graphics g)
        {
            bool valid = false;
            if (parameters.Split(',').Length > 1)
            {
                int width = int.Parse(parameters.Split(',')[0].Trim());
                int height = int.Parse(parameters.Split(',')[1].Trim());
                Shape rectangle = sfactory.getShape("RECTANGLE");
                rectangle.set(color, x, y, width, height);
                rectangle.draw(g, fill);
                valid = true;
            }
            else
            {
                list.Add("Command is required");
            }
            return valid;
        }

        /// <summary>
        /// method to draw triangle
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="sfactory"></param>
        /// <param name="g"></param>
        /// <returns></returns>
        static bool drawTriangle(String parameters, ShapeFactory sfactory, Graphics g)
        {
            bool valid = false;
            if (parameters.Split(',').Length > 3)
            {
                int x2 = int.Parse(parameters.Split(',')[0].Trim());
                int y2 = int.Parse(parameters.Split(',')[1].Trim());
                int x3 = int.Parse(parameters.Split(',')[2].Trim());
                int y3 = int.Parse(parameters.Split(',')[3].Trim());
                Shape triangle = sfactory.getShape("TRIANGLE");
                triangle.set(color, x, y, x2, y2, x3, y3);
                triangle.draw(g, fill);
                valid = true;
            }
            else
            {
                list.Add("Command is required");
            }
            return valid;
        }
    }

}
