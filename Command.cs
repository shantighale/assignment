﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WindowsForms77229120
{
    class Command
    {
        /// <summary>
        /// check radius of circle
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static bool checkRad(String param)
        {
            bool status = false;
            if (param.Contains(","))
                status = false;
            else status = true;

            return status;
        }

        /// <summary>
        /// check if variable exist in list
        /// </summary>
        /// <param name="variables"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static bool checkIfVariableExist(Hashtable variables, String param)
        {
            bool status = false;
            if (variables.ContainsKey(param))
            {
                status = true;
            }
            return status;
        }

        /// <summary>
        /// get value from list
        /// </summary>
        /// <param name="variables"></param>
        /// <param name="vars"></param>
        /// <returns></returns>
        public static string getFrom(Hashtable variables, String vars)
        {
            ICollection key = variables.Keys;

            foreach (string k in key)
            {
                Match m = Regex.Match(vars, @"\b" + k + @"\b");
                if (m.Success)
                {
                    vars = Regex.Replace(vars, @"\b" + k + @"\b", variables[k].ToString());
                }
            }
            return vars;
        }


        /// <summary>
        /// check variable in list
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public static bool checkVar(String command)
        {
            bool status = false;
            if (Regex.IsMatch(command, "^[a-zA-z]+\\s?[=]\\s?[\\d]+"))
            {
                status = true;
            }
            return status;
        }
    }
}
