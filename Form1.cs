﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms77229120
{
    public partial class Form1 : Form
    {
        Graphics g;

        public Form1()
        {
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
        }
        public String Message()
        {
            String message = "";
            for (int i = 0; i < CmdParse.mesglst.Count; i++)
            {
                message += CmdParse.mesglst[i] + "\n";
            }
            return message;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Text Files(*.txt)|*.txt| All Files(*.*)|*.*";
            saveFile.FilterIndex = 2;
            if (saveFile.ShowDialog() == DialogResult.OK)
            {

                StreamWriter saveStream = new StreamWriter(File.Create(saveFile.FileName));
                saveStream.Write(CmdBox.Text);
                saveStream.Dispose();
            }
        }
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.InitialDirectory = "c:\\";
            openFile.Filter = "Text Files(*.txt)|*.txt| All Files(*.*)|*.*";
            openFile.FilterIndex = 2;
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                ////Get the path of specified file
                StreamReader openStream = new StreamReader(File.OpenRead(openFile.FileName));
                CmdBox.Text = openStream.ReadToEnd();
                openStream.Dispose();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text))
            {
                textBox2.Text = "Command is required.";
            }
            else
            if (String.IsNullOrEmpty(CmdBox.Text))
            {
                textBox2.Text = "Command is required.";
            }
            else if (textBox1.Text.ToUpper().Equals("RUN"))
            {
                ParseCmd pars = new ParseCmd(textBox2);
                pars.execute(CmdBox.Text, g);
            }
            else if (textBox1.Text.ToUpper().Equals("RESET"))
            {
                textBox1.Text = "";
                CmdBox.Text = "";
                textBox2.Text = "";
                pictureBox1.Image = null;
                textBox1.Focus();
                CmdParse.clearShape();
            }
            else if (textBox1.Text.ToUpper().Equals("CLEAR"))
            {
                textBox1.Text = "";
                CmdBox.Text = "";
                textBox2.Text = "";
                pictureBox1.Image = null;
                textBox1.Focus();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            CmdBox.Clear();
            pictureBox1.Refresh();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void CmdBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
