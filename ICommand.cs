﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms77229120
{
    /// <summary>
    /// interface 
    /// </summary>
    interface ICommand
    {
        void draw();
        void setFlash(bool status, String color);
    }
}
