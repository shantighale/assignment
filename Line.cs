﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms77229120
{
    internal class Line : ICommand
    {
        /// <summary>
        /// variable declaration
        /// </summary>
        private Color color;
        private int xAxis, yAxis, x, y;
        Graphics g;

        /// <summary>
        /// parameterized constructor
        /// </summary>
        /// <param name="color"></param>
        /// <param name="xAxis"></param>
        /// <param name="yAxis"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="graphics"></param>
        public Line(Color color, int xAxis, int yAxis, int x, int y, Graphics graphics)
        {
            this.color = color;
            this.xAxis = xAxis;
            this.yAxis = yAxis;
            this.x = x;
            this.y = y;
            this.g = graphics;
        }

        /// <summary>
        /// method to draw line
        /// </summary>
        public void draw()
        {
            Pen pen = new Pen(color, 2);
            g.DrawLine(pen, xAxis, yAxis, x, y);
        }

        public void setFlash(bool status, string color)
        {

        }
    }

}
