﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms77229120
{
    internal class NewCircle : ICommand
    {
        /// <summary>
        /// variable declaration
        /// </summary>
        private Color color;
        private int xAxis, yAxis, radius, fill;
        private Graphics graphics;
        bool isThread = false;
        String colorCase = "";

        /// <summary>
        /// parameterized constructor
        /// </summary>
        /// <param name="color"></param>
        /// <param name="xAxis"></param>
        /// <param name="yAxix"></param>
        /// <param name="radius"></param>
        /// <param name="fill"></param>
        /// <param name="graphics"></param>
        public NewCircle(Color color, int xAxis, int yAxix, int radius, int fill, Graphics graphics)
        {
            this.color = color;
            this.xAxis = xAxis;
            this.yAxis = yAxix;
            this.radius = radius;
            this.graphics = graphics;
            this.fill = fill;
        }

        /// <summary>
        /// method to set values to variables
        /// </summary>
        /// <param name="status"></param>
        /// <param name="color"></param>
        public void setFlash(bool status, string color)
        {
            isThread = status;
            colorCase = color;
        }

        /// <summary>
        /// method to draw circle
        /// </summary>
        public void draw()
        {
            ShapeFactory factory = new ShapeFactory();
            Shape shape = factory.getShape("CIRCLE");
            switch (isThread)
            {
                case true:
                    drawAsyncCircle();
                    break;
                case false:
                    shape.set(color, xAxis, yAxis, radius);
                    shape.draw(graphics, fill);
                    break;
                default:
                    shape.set(color, xAxis, yAxis, radius);
                    shape.draw(graphics, fill);
                    break;
            }

        }


        /// <summary>
        /// method to draw circle
        /// </summary>
        /// <returns></returns>
        public async Task drawAsyncCircle()
        {
            switch (colorCase)
            {
                case "REDGREEN":
                    bool redgreen = false;
                    while (true)
                    {
                        while (!redgreen)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape circle = factory.getShape("CIRCLE");
                                circle.set(Color.Red, xAxis, yAxis, radius);
                                circle.draw(graphics, fill);
                                redgreen = true;
                                System.Threading.Thread.Sleep(500);
                            });
                        }

                        while (redgreen)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape circle = factory.getShape("CIRCLE");
                                circle.set(Color.Green, xAxis, yAxis, radius);
                                circle.draw(graphics, fill);
                                redgreen = false;
                                System.Threading.Thread.Sleep(500);
                            });
                        }
                    }
                    break;
                case "BLUEYELLOW":
                    bool blueyellow = false;
                    while (true)
                    {
                        while (!blueyellow)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape circle = factory.getShape("CIRCLE");
                                circle.set(Color.Blue, xAxis, yAxis, radius);
                                circle.draw(graphics, fill);
                                blueyellow = true;
                                System.Threading.Thread.Sleep(500);
                            });
                        }

                        while (blueyellow)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape circle = factory.getShape("CIRCLE");
                                circle.set(Color.Yellow, xAxis, yAxis, radius);
                                circle.draw(graphics, fill);
                                blueyellow = false;
                                System.Threading.Thread.Sleep(500);
                            });
                        }
                    }
                    break;
                case "BLACKWHITE":
                    bool blackwhite = false;
                    while (true)
                    {
                        while (!blackwhite)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape circle = factory.getShape("CIRCLE");
                                circle.set(Color.Black, xAxis, yAxis, radius);
                                circle.draw(graphics, fill);
                                blackwhite = true;
                                System.Threading.Thread.Sleep(500);
                            });
                        }

                        while (blackwhite)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape circle = factory.getShape("CIRCLE");
                                circle.set(Color.White, xAxis, yAxis, radius);
                                circle.draw(graphics, fill);
                                blackwhite = false;
                                System.Threading.Thread.Sleep(500);
                            });
                        }
                    }
                    break;
            }
        }

    }
}
