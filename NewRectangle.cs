﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms77229120
{
    internal class NewRectangle : ICommand
    {
        /// <summary>
        /// variable declaration
        /// </summary>
        private Color color = Color.Empty;
        private int xAxis, yAxis, fill, width, height;
        private Graphics graphics;
        bool isThread = false;
        String colorCase = "";

        /// <summary>
        /// parameterized constructor
        /// </summary>
        /// <param name="color"></param>
        /// <param name="xAxis"></param>
        /// <param name="yAxis"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="fill"></param>
        /// <param name="graphics"></param>
        public NewRectangle(Color color, int xAxis, int yAxis, int width, int height, int fill, Graphics graphics)
        {
            this.color = color;
            this.xAxis = xAxis;
            this.yAxis = yAxis;
            this.width = width;
            this.height = height;
            this.graphics = graphics;
            this.fill = fill;
        }

        /// <summary>
        /// method to set values to varaible
        /// </summary>
        /// <param name="status"></param>
        /// <param name="color"></param>
        public void setFlash(bool status, String color)
        {
            isThread = status;
            colorCase = color;
        }

        /// <summary>
        /// method to draw rectangle
        /// </summary>
        /// <returns></returns>
        public async Task drawAsyncRectangle()
        {
            switch (colorCase)
            {
                case "REDGREEN":
                    bool redgreen = false;
                    while (true)
                    {
                        while (!redgreen)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape shape = factory.getShape("RECTANGLE");
                                shape.set(Color.Red, xAxis, yAxis, width, height);
                                shape.draw(graphics, fill);
                                redgreen = true;
                                System.Threading.Thread.Sleep(500);
                            });
                        }

                        while (redgreen)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape shape = factory.getShape("RECTANGLE");
                                shape.set(Color.Green, xAxis, yAxis, width, height);
                                shape.draw(graphics, fill);
                                redgreen = false;
                                System.Threading.Thread.Sleep(500);
                            });
                        }
                    }
                    break;
                case "BLUEYELLOW":
                    bool blueyellow = false;
                    while (true)
                    {
                        while (!blueyellow)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape shape = factory.getShape("RECTANGLE");
                                shape.set(Color.Blue, xAxis, yAxis, width, height);
                                shape.draw(graphics, fill);
                                blueyellow = true;
                                System.Threading.Thread.Sleep(500);
                            });
                        }

                        while (blueyellow)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape shape = factory.getShape("RECTANGLE");
                                shape.set(Color.Yellow, xAxis, yAxis, width, height);
                                shape.draw(graphics, fill);
                                blueyellow = false;
                                System.Threading.Thread.Sleep(500);
                            });
                        }
                    }
                    break;
                case "BLACKWHITE":
                    bool blackwhite = false;
                    while (true)
                    {
                        while (!blackwhite)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape shape = factory.getShape("RECTANGLE");
                                shape.set(Color.Black, xAxis, yAxis, width, height);
                                shape.draw(graphics, fill);
                                blackwhite = true;
                                System.Threading.Thread.Sleep(500);
                            });
                        }

                        while (blackwhite)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory factory = new ShapeFactory();
                                Shape shape = factory.getShape("RECTANGLE");
                                shape.set(Color.White, xAxis, yAxis, width, height);
                                shape.draw(graphics, fill);
                                blackwhite = false;
                                System.Threading.Thread.Sleep(500);
                            });
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// method to draw rectangle
        /// </summary>
        public void draw()
        {
            ShapeFactory factory = new ShapeFactory();
            Shape shape = factory.getShape("RECTANGLE");
            switch (isThread)
            {
                case true:
                    drawAsyncRectangle();
                    break;
                case false:
                    shape.set(color, xAxis, yAxis, width, height);
                    shape.draw(graphics, fill);
                    break;
                default:
                    shape.set(color, xAxis, yAxis, width, height);
                    shape.draw(graphics, fill);
                    break;
            }
        }
    }
}
