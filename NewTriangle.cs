﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms77229120
{
    class NewTriangle : ICommand
    {
        /// <summary>
        /// variable declaration
        /// </summary>
        int x2, y2, x3, y3;
        int x, y;
        double a, b, c;
        Color colour;
        int fill = 0;
        Graphics graphics;
        bool isThread = false;
        String colorCase = "";
        public NewTriangle(Color color, int x, int y, int x2, int y2, int x3, int y3, int isFill, Graphics g)
        {
            this.x2 = x2;
            this.y2 = y2;
            this.x3 = x3;
            this.y3 = y3;
            this.x = x;
            this.y = y;
            this.a = Math.Sqrt((x2 - x) ^ 2 + (y2 - y) ^ 2);
            this.b = Math.Sqrt((x3 - x2) ^ 2 + (y3 - y2) ^ 2);
            this.c = Math.Sqrt((x - x3) ^ 2 + (y - y3) ^ 2);
            this.colour = color;
            this.fill = isFill;
            this.graphics = g;
        }

        /// <summary>
        /// method to declare value to varialbes
        /// </summary>
        /// <param name="status"></param>
        /// <param name="color"></param>
        public void setFlash(bool status, string color)
        {
            isThread = status;
            colorCase = color;
        }

        /// <summary>
        /// draw triangle
        /// </summary>
        public void draw()
        {
            ShapeFactory sfactory = new ShapeFactory();
            Shape triangle = sfactory.getShape("TRIANGLE");
            switch (isThread)
            {
                case true:
                    drawAsyncTriangle();
                    break;
                case false:
                    triangle.set(colour, x, y, x2, y2, x3, y3);
                    triangle.draw(graphics, fill);
                    break;
                default:
                    triangle.set(colour, x, y, x2, y2, x3, y3);
                    triangle.draw(graphics, fill);
                    break;
            }

        }

        /// <summary>
        /// draw triangle
        /// </summary>
        /// <returns></returns>
        public async Task drawAsyncTriangle()
        {
            switch (colorCase)
            {
                case "REDGREEN":
                    bool redgreen = false;
                    while (true)
                    {
                        while (!redgreen)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory sfactory = new ShapeFactory();
                                Shape triangle = sfactory.getShape("TRIANGLE");
                                triangle.set(Color.Red, x, y, x2, y2, x3, y3);
                                triangle.draw(graphics, fill);
                                redgreen = true;
                                System.Threading.Thread.Sleep(500);
                            });
                        }

                        while (redgreen)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory sfactory = new ShapeFactory();
                                Shape triangle = sfactory.getShape("TRIANGLE");
                                triangle.set(Color.Green, x, y, x2, y2, x3, y3);
                                triangle.draw(graphics, fill);
                                redgreen = false;
                                System.Threading.Thread.Sleep(500);
                            });
                        }
                    }
                    break;
                case "BLUEYELLOW":
                    bool blueyellow = false;
                    while (true)
                    {
                        while (!blueyellow)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory sfactory = new ShapeFactory();
                                Shape triangle = sfactory.getShape("TRIANGLE");
                                triangle.set(Color.Blue, x, y, x2, y2, x3, y3);
                                triangle.draw(graphics, fill);
                                blueyellow = true;
                                System.Threading.Thread.Sleep(500);
                            });
                        }

                        while (blueyellow)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory sfactory = new ShapeFactory();
                                Shape triangle = sfactory.getShape("TRIANGLE");
                                triangle.set(Color.Yellow, x, y, x2, y2, x3, y3);
                                triangle.draw(graphics, fill);
                                blueyellow = false;
                                System.Threading.Thread.Sleep(500);
                            });
                        }
                    }
                    break;
                case "BLACKWHITE":
                    bool blackwhite = false;
                    while (true)
                    {
                        while (!blackwhite)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory sfactory = new ShapeFactory();
                                Shape triangle = sfactory.getShape("TRIANGLE");
                                triangle.set(Color.Black, x, y, x2, y2, x3, y3);
                                triangle.draw(graphics, fill);
                                blackwhite = true;
                                System.Threading.Thread.Sleep(500);
                            });
                        }

                        while (blackwhite)
                        {
                            await Task.Run(() =>
                            {
                                ShapeFactory sfactory = new ShapeFactory();
                                Shape triangle = sfactory.getShape("TRIANGLE");
                                triangle.set(Color.White, x, y, x2, y2, x3, y3);
                                triangle.draw(graphics, fill);
                                blackwhite = false;
                                System.Threading.Thread.Sleep(500);
                            });
                        }
                    }
                    break;
            }
        }

    }
}
