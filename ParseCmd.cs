﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms77229120
{
    public class ParseCmd
    {
        /// <summary>
        /// variable declaration
        /// </summary>
        private static int x = 0, y = 0;
        private static Color color = Color.Black;
        public static List<String> list = new List<string>();
        public static int fill = 0;
        public static List<String> mglst = new List<string>();
        static Hashtable var = new Hashtable();
        Graphics g;

        bool thread = false;
        String cCase = "";

        TextBox richtextbox;
        public ParseCmd(TextBox richbox)
        {
            this.richtextbox = richbox;
        }
        //method to execute command
        public void execute(string comd, Graphics graphics)
        {
            this.g = graphics;
            int IF = 0;
            int METHOD = 0;
            List<String> IfArray = new List<String>();
            List<String> methodLst = new List<String>();
            List<String> loopLst = new List<String>();
            String IfVariable = "", IfOperator = "", IfNumber = "";

            char[] delimiters = new char[] { '\r', '\n' };
            string[] cmd = comd.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            bool hasParam = false;
            String methodName = "";
            String[] paramss = { };
            int WHILE = 0;
            String wV = "";
            String wO = "";
            String wN = "";

            String line = "";

            for (int i = 0; i < cmd.Length; i++)
            {
                line = (i + 1).ToString();
                String[] pre = cmd[i].Split(' ');


                if (Command.checkVar(cmd[i]))
                {
                    string[] param = Array.ConvertAll(cmd[i].Trim().Split('='), s => s);
                    String key = param[0].Trim();
                    String value = param[1].Trim();


                    if (var.ContainsKey(key))
                    {
                        var[key] = value;
                    }
                    else
                    {
                        var.Add(key, value);
                    }
                }
                else
                {
                    //IF Condition
                    if (pre[0].ToUpper() == "IF")
                    {
                        IF = 1;
                        IfVariable = pre[1];
                        IfOperator = pre[2];
                        IfNumber = (pre[3]);
                    }
                    else if (IF == 1 && pre[0].ToUpper() != "ENDIF")
                    {
                        IfArray.Add(cmd[i]);
                    }
                    else if (pre[0].ToUpper() == "ENDIF")
                    {
                        if (var.ContainsKey(IfVariable))
                        {
                            switch (IfOperator)
                            {
                                case "==":
                                    if (Command.getFrom(var, IfVariable) == IfNumber)
                                    {
                                        foreach (String item in IfArray)
                                        {
                                            parse(item, line);
                                        }
                                    }
                                    break;

                                case ">":
                                    if (int.Parse(Command.getFrom(var, IfVariable)) > int.Parse(IfNumber))
                                    {
                                        foreach (String item in IfArray)
                                        {
                                            parse(item, line);
                                        }
                                    }
                                    break;
                                case "<":
                                    if (int.Parse(Command.getFrom(var, IfVariable)) < int.Parse(IfNumber))
                                    {
                                        foreach (String item in IfArray)
                                        {
                                            parse(item, line);
                                        }
                                    }
                                    break;
                                case ">=":
                                    if (int.Parse(Command.getFrom(var, IfVariable)) >= int.Parse(IfNumber))
                                    {
                                        foreach (String item in IfArray)
                                        {
                                            parse(item, line);
                                        }
                                    }
                                    break;
                                case "<=":
                                    if (int.Parse(Command.getFrom(var, IfVariable)) <= int.Parse(IfNumber))
                                    {
                                        foreach (String item in IfArray)
                                        {
                                            parse(item, line);
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    else if (pre[0].ToUpper() == "METHOD")
                    {
                        METHOD = 1;
                        methodName = pre[1].Trim();
                        if (methodName.Contains("(") && methodName.Contains(")"))
                        {
                            hasParam = true;
                            String[] par = pre[1].Split('(');
                            par[1] = par[1].Replace(")", "").ToString();
                            paramss = par[1].Split(',');
                            foreach (String s in paramss)
                            {
                                if (!var.ContainsKey(s))
                                {
                                    var.Add(s, "0");
                                }
                            }
                        }
                        else
                        {
                            hasParam = false;
                            methodName = pre[1].Trim();
                        }
                    }
                    else if (METHOD == 1 && pre[0].ToUpper() != "ENDMETHOD")
                    {
                        methodLst.Add(cmd[i]);
                    }
                    else if (pre[0].ToUpper() == "ENDMETHOD")
                    {
                        METHOD = 0;
                    }
                    else if (cmd[i].ToUpper() == "START " + methodName.ToUpper())
                    {
                        if (hasParam)
                        {
                            if (methodName.Contains("(") && methodName.Contains(")"))
                            {
                                hasParam = true;
                                String[] para = methodName.Split('(');
                                para[1] = para[1].Replace(")", "").ToString();
                                String[] parama = para[1].Split(',');
                                int contain = 0;
                                foreach (String s in parama)
                                {

                                    if (var.ContainsKey(s))
                                    {
                                        contain++;
                                    }
                                }
                                if (contain == parama.Count())
                                {
                                    foreach (String s in parama)
                                    {
                                        var[s] = Command.getFrom(var, s);
                                    }
                                    foreach (String commands in methodLst)
                                    {
                                        parse(commands, line);
                                    }
                                }
                            }

                        }
                        else
                        {
                            foreach (String commands in methodLst)
                            {
                                parse(commands, line);
                            }
                        }

                    }
                    else if (pre[0].ToUpper() == "WHILE")
                    {
                        WHILE = 1;
                        wV = pre[1];
                        wO = pre[2];
                        wN = pre[3];
                    }
                    else if (WHILE == 1 && pre[0].ToUpper() != "ENDWHILE")
                    {
                        loopLst.Add(cmd[i]);
                    }
                    else if (pre[0].ToUpper() == "ENDWHILE")
                    {
                        if (var.ContainsKey(wV))
                        {
                            int loopVar = int.Parse(Command.getFrom(var, wV));
                            switch (wO)
                            {
                                case "<":
                                    while (loopVar < int.Parse(wN))
                                    {
                                        foreach (String s in loopLst)
                                        {
                                            parse(s, line);
                                        }
                                        loopVar = loopVar + 1;
                                        var[wV] = loopVar;
                                    }
                                    break;

                                case ">":
                                    while (loopVar > int.Parse(wN))
                                    {
                                        foreach (String s in loopLst)
                                        {
                                            parse(s, line);
                                        }
                                        loopVar = loopVar - 1;
                                        var[wV] = loopVar;
                                    }
                                    break;
                            }
                        }
                        else
                        {

                        }

                    }
                    else
                    {
                        parse(cmd[i], line);
                    }
                }
            }

        }

        /// <summary>
        /// method to parse command
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool parse(string cmd, String line)
        {
            bool status = false;
            mglst.Clear();
            if (Command.checkVar(cmd))
            {
                string[] param = Array.ConvertAll(cmd.ToLower().Trim().Split('='), s => s);
                if (Command.checkIfVariableExist(var, param[0]))
                {
                    var[param[0]] = param[1];
                    status = true;
                }
                else
                {
                    var.Add(param[0], param[1]);
                    status = true;
                }
            }
            else
            {
                string[] args = cmd.Split(new[] { ' ' });
                String command = args[0].ToUpper().Trim();
                ShapeFactory factory = new ShapeFactory();
                try
                {

                    if (args.Length > 1)
                    {

                        switch (command)
                        {
                            case "":
                                status = false;
                                //Replace the value in the message box with the value from below, and then remove the message box.
                                mglst.Add("Command is required.");

                                break;
                            case "DRAWTO":
                                String[] parDraw = args[1].Split(',');
                                if (parDraw.Length == 2)
                                {
                                    int xc = 0, yc = 0;
                                    if (int.TryParse(parDraw[0], out xc) && int.TryParse(parDraw[1], out yc))
                                    {
                                        ICommand cmdi = new Line(color, x, y, xc, yc, g);
                                        cmdi.draw();
                                        status = true;
                                    }
                                    else
                                    {

                                        if (Command.checkIfVariableExist(var, parDraw[0]) && Command.checkIfVariableExist(var, parDraw[1]))
                                        {
                                            ICommand cmdi = new Line(color, x, y, int.Parse(Command.getFrom(var, parDraw[0].ToString())), int.Parse(Command.getFrom(var, parDraw[1].ToString())), g);
                                            cmdi.draw();
                                            status = true;
                                        }
                                        else
                                        {


                                        }

                                    }

                                }
                                else
                                {

                                }

                                break;
                            case "CIRCLE":
                                if (Command.checkRad(args[1]))
                                {
                                    int radius = 0;
                                    if (int.TryParse(args[1], out radius))
                                    {
                                        ICommand cmdi = new NewCircle(color, x, y, radius, fill, g);
                                        cmdi.setFlash(thread, cCase);
                                        cmdi.draw();
                                        status = true;
                                    }
                                    else
                                    {
                                        if (Command.checkIfVariableExist(var, args[1]))
                                        {
                                            radius = int.Parse(Command.getFrom(var, args[1]));
                                            ICommand cmdi = new NewCircle(color, x, y, radius, fill, g);
                                            cmdi.setFlash(thread, cCase);
                                            cmdi.draw();
                                            status = true;
                                        }
                                    }
                                }

                                break;
                            case "MOVETO":

                                if (args.Length == 2)
                                {
                                    String p1 = args[1].Split(',')[0].Trim();
                                    String p2 = args[1].Split(',')[1].Trim();

                                    int xc = 0, yc = 0;
                                    if (int.TryParse(p1, out xc) && int.TryParse(p2, out yc))
                                    {
                                        x = xc;
                                        y = yc;
                                    }
                                    else
                                    {
                                        if (Command.checkIfVariableExist(var, p1) && Command.checkIfVariableExist(var, p2))
                                        {
                                            x = int.Parse(Command.getFrom(var, p1));
                                            y = int.Parse(Command.getFrom(var, p2));
                                            status = true;
                                        }
                                        else
                                        {


                                        }
                                    }

                                }
                                else
                                {

                                }

                                break;
                            case "RECTANGLE":
                                String[] paramRect = args[1].Split(',');
                                if (paramRect.Length == 2)
                                {
                                    int xa = 0, ya = 0;
                                    if (int.TryParse(paramRect[0], out xa) && int.TryParse(paramRect[1], out ya))
                                    {
                                        ICommand cmdi = new NewRectangle(color, x, y, xa, ya, fill, g);
                                        cmdi.setFlash(thread, cCase);
                                        cmdi.draw();
                                        status = true;
                                    }
                                    else
                                    {
                                        if (Command.checkIfVariableExist(var, paramRect[0]) && Command.checkIfVariableExist(var, paramRect[1]))
                                        {
                                            xa = int.Parse(Command.getFrom(var, paramRect[0]));
                                            ya = int.Parse(Command.getFrom(var, paramRect[1]));
                                            ICommand cmdi = new NewRectangle(color, x, y, xa, ya, fill, g);
                                            cmdi.setFlash(thread, cCase);
                                            cmdi.draw();
                                            status = true;
                                        }
                                        else
                                        {
                                            // invalid parameter
                                        }

                                    }

                                }
                                else
                                {
                                    //not enough parameter
                                }

                                break;
                            case "TRIANGLE":
                                String[] paramTrian = args[1].Split(',');
                                if (paramTrian.Length == 4)
                                {
                                    int xa = 0, ya = 0, za = 0, aa = 0;
                                    if (int.TryParse(paramTrian[0], out xa) && int.TryParse(paramTrian[1], out ya) && int.TryParse(paramTrian[2], out za) && int.TryParse(paramTrian[3], out aa))
                                    {
                                        ICommand cmdi = new NewTriangle(color, x, y, xa, ya, za, aa, fill, g);
                                        cmdi.setFlash(thread, cCase);
                                        cmdi.draw();
                                        status = true;
                                    }
                                    else
                                    {
                                        if (Command.checkIfVariableExist(var, paramTrian[0]) && Command.checkIfVariableExist(var, paramTrian[1]) && Command.checkIfVariableExist(var, paramTrian[2]) && Command.checkIfVariableExist(var, paramTrian[3]))
                                        {
                                            xa = int.Parse(Command.getFrom(var, paramTrian[0]));
                                            ya = int.Parse(Command.getFrom(var, paramTrian[1]));
                                            za = int.Parse(Command.getFrom(var, paramTrian[2]));
                                            aa = int.Parse(Command.getFrom(var, paramTrian[3]));
                                            ICommand cmdi = new NewTriangle(color, x, y, xa, ya, za, aa, fill, g);
                                            cmdi.setFlash(thread, cCase);
                                            cmdi.draw();
                                            status = true;
                                        }
                                        else
                                        {
                                            // invalid parameter
                                        }

                                    }

                                }
                                else
                                {
                                    //not enough parameter
                                }

                                break;
                            case "PEN":
                                switch (args[1].ToUpper().Trim())
                                {
                                    case "":
                                        status = false;
                                        mglst.Add("Shape is required.");
                                        break;
                                    case "RED":
                                        color = Color.FromName("red");
                                        status = true;
                                        break;
                                    case "BLACK":
                                        color = Color.FromName("black");
                                        status = true;
                                        break;
                                    case "GREEN":
                                        color = Color.FromName("green");
                                        status = true;
                                        break;
                                    case "REDGREEN":
                                        thread = true;
                                        cCase = "REDGREEN";
                                        status = true;
                                        break;
                                    case "BLUEYELLOW":
                                        thread = true;
                                        cCase = "BLUEYELLOW";
                                        status = true;
                                        break;
                                    case "BLACKWHITE":
                                        thread = true;
                                        cCase = "BLACKWHITE";
                                        status = true;
                                        break;
                                    default:
                                        status = false;
                                        mglst.Add("Colour not found.");
                                        break;
                                }
                                break;
                            case "FILL":
                                switch (args[1].ToUpper().Trim())
                                {
                                    case "":
                                        //
                                        status = false;
                                        break;
                                    case "ON":
                                        fill = 1;
                                        status = true;
                                        break;
                                    case "OFF":
                                        fill = 0;
                                        status = true;
                                        break;
                                    default:
                                        status = false;
                                        //add msg box value here which is below and remove message box and same apply for all
                                        mglst.Add("Invalid command");
                                        break;
                                }
                                break;
                            default:
                                status = false;
                                //add msg box value here which is below and remove message box and same apply for all
                                mglst.Add("Command not found");
                                break;
                        }
                    }
                    else
                    {
                        status = false;
                        //Replace the value in the message box with the value from below, and then remove the message box.
                        mglst.Add("Command not found.");
                    }
                }
                catch (FormatException)
                {
                    status = false;
                    //Replace the value in the message box with the value from below, and then remove the message box.
                    mglst.Add("Invalid argument type");
                }
                catch (Exception)
                {
                    status = false;
                    //Replace the value in the message box with the value from below, and then remove the message box.
                    mglst.Add("Invalid argument type");
                }
            }

            return status;
        }


        /// <summary>
        /// method to clear content
        /// </summary>
        public static void clearShape()
        {
            x = 0;
            y = 0;
            color = Color.Black;
            fill = 0;
        }
    }
}
