﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms77229120
{
    class Rectangle:Shape
    {
        int width, height;
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }
        public Rectangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {
            //the only thingthat is different from shape
            this.width = width; 
            this.height = height;
        }

        public override void set(Color colour, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is width, list[3] is height
            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];

        }

        public override void draw(Graphics g, int fill = 0)
        {
            Pen p = new Pen(Color.Black, 2);
            SolidBrush b = new SolidBrush(colour);
            switch(fill)
            {
                case 0:
                    g.DrawRectangle(p, x, y, width, height);
                    break;
                case 1:
                    g.FillRectangle(b, x, y, width, height);
                    break;
                default:
                    g.DrawRectangle(p, x, y, width, height);
                    break;
            }
        }

        public override double calcArea()
        {
            return width * height;
        }

        public override double calcPerimeter()
        {
            return 2 * width + 2 * height;
        }
    }
}
