﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestTriangle()
        {
            //Arrange
            String cmd = "draw triangle 10,10,10,10";
            Bitmap bitmap = new Bitmap(100, 100);
            Graphics g = Graphics.FromImage(bitmap);

            //Act
            bool result = WindowsFormsApp3.CommandParser.parse(cmd, g);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestCircle()
        {
            //Arrange
            String cmd = "draw circle 10";
            Bitmap bitmap = new Bitmap(100, 100);
            Graphics g = Graphics.FromImage(bitmap);

            //Act
            bool result = WindowsFormsApp3.CommandParser.parse(cmd, g);

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestMoveTo()
        {
            //Arrange
            String cmd = "moveto 10,20";
            Bitmap bitmap = new Bitmap(100, 100);
            Graphics g = Graphics.FromImage(bitmap);

            //Act
            bool result = WindowsFormsApp3.CommandParser.parse(cmd, g);

            //Assert
            Assert.IsTrue(result);
        }
    }
}
